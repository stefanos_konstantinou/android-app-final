This privacy policy has been compiled to better serve those who are concerned with how their 'Personally identifiable information' (PII) is being used online. PII, as used in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our application.

What personal information do we collect from the people that use our app?

When entering the specified email that you want the location of your phone to be sent at by a word the user has specified.

When do we collect information?

We collect information from you when you enter information in Setup.

How do we use your information?
We use the information we collect from you when you send the specific message you have set in order to activate the application.

Third-party disclosure

We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information.

Third-party links

We do not include or offer third-party products or services on our website.

Google

Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en 
We have not enabled Google AdSense on our site but we may do so in the future.

COPPA (Children Online Privacy Protection Act)

When it comes to the collection of personal information from children under 13, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, the nation's consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.

We adhere to the following COPPA tenants:
      � Parents can review, delete, manage or refuse with whom their child's information is shared through contacting us directly.
or contacting us directly.


CAN SPAM Act

The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.
We collect your email address in order to: 
 
We will only email you only when you enable the feature by sending the specific message you have set.
If at any time you would like to unsubscribe from receiving future emails, you can email us at droidapptest94@gmail.com
and we will promptly remove you from ALL correspondence.
If there are any questions regarding this privacy policy you may contact us using the information below.    Last Edited on 2016-03-27
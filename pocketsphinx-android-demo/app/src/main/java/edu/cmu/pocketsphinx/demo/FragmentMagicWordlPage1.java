package edu.cmu.pocketsphinx.demo;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FragmentMagicWordlPage1 extends Fragment implements View.OnClickListener {
    CheckBox messageReceive;
    String filename = "magicword.txt";
    String MagicWord;
    String onOrOff;
    Boolean hasFlash;
    public  static Camera camera;
    FileOutputStream OutputStream;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onSubmitMagicWord();
        messageReceive = (CheckBox) getActivity().findViewById(R.id.messageReceive);
        checkMessage();
        SharedPreferences prefs = getActivity().getSharedPreferences("edu.cmu.pocketsphinx.demo", getActivity().MODE_PRIVATE);
        boolean checkbox = prefs.getBoolean("checkbox_status", false);

        if (checkbox) {
            messageReceive.setChecked(true);
        } else {
            messageReceive.setChecked(false);
        }



    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_1, container, false);

        return rootview;
    }

    private void onSubmitMagicWord() {
        Button Submit = (Button) getActivity().findViewById(R.id.SubmitButton);
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //on click for submitting the word we check if the word is not empty and
                //if its not is set as default word to enable the whole process in the Main activity(OnReceiving())
                EditText magic = (EditText) getActivity().findViewById(R.id.editSpecialWord);
                try {
                    if (magic.length() == 0) {
                        Toast.makeText(getActivity().getApplicationContext(), "Please enter a word!", Toast.LENGTH_SHORT).show();
                    } else {
                        MagicWord = magic.getText().toString();
                        magic.setText("");

                        FileOutputStream OutputStream;
                        OutputStream = getActivity().openFileOutput(filename, Context.MODE_PRIVATE);
                        OutputStream.write(MagicWord.getBytes());
                        OutputStream.close();
                        Toast.makeText(getActivity().getApplicationContext(), MagicWord + " is now your default word", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getActivity().getApplicationContext(), "An error occured,Please try again!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void checkMessage() {
        messageReceive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //on click we check to see whether the user wants music to be played on message receive that
                //matches the pre-set word in order to enable it
                FileOutputStream OutputStream = null;
                try {
                    OutputStream = getActivity().openFileOutput("musicbool.txt", Context.MODE_PRIVATE);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                if (messageReceive.isChecked()) {
                    onOrOff = "1";
                } else {
                    onOrOff = "0";
                }
                try {

                    OutputStream.write(onOrOff.getBytes());
                    OutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                SharedPreferences.Editor editor = getActivity().getSharedPreferences("edu.cmu.pocketsphinx.demo", getActivity().MODE_PRIVATE).edit();
                editor.putBoolean("checkbox_status", messageReceive.isChecked());
                editor.commit();
            }
        });
    }


    @Override
    public void onClick(View v) {

    }
}
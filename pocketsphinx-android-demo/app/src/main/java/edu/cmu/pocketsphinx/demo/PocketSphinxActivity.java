/* ====================================================================
 * Copyright (c) 2014 Alpha Cephei Inc.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALPHA CEPHEI INC. ``AS IS'' AND
 * ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY
 * NOR ITS EMPLOYEES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ====================================================================
 */

package edu.cmu.pocketsphinx.demo;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;

import static android.widget.Toast.makeText;
import static edu.cmu.pocketsphinx.SpeechRecognizerSetup.defaultSetup;

public class PocketSphinxActivity extends Activity implements
        RecognitionListener {

   /* @Override
    public  boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_options,menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    /* Named searches allow to quickly reconfigure the decoder */
    private static final String KWS_SEARCH = "wake up";
    private static final String Music_stop = "music stop";
    /* Keyword we are looking for to activate menu */
    private static final String KEYPHRASE = "oh mighty computer";
    String bool;
    private TextView switchstatus;
    private Switch myswitch;
    Vibrator v;
    Camera camera;
    AudioManager audioManager;
    private SpeechRecognizer recognizer;
    private HashMap < String, Integer > captions;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);


        // Prepare the data for UI
        captions = new HashMap < String, Integer > ();
        captions.put(KWS_SEARCH, R.string.kws_caption);
        setContentView(R.layout.main);
        // Recognizer initialization is a time-consuming and it involves IO,
        // so we execute it in async task
        switchstatus = (TextView) findViewById(R.id.recognOnOrOff);
        myswitch = (Switch) findViewById(R.id.recognOnOrOff);
        Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        SharedPreferences prefs = getSharedPreferences("edu.cmu.pocketsphinx.demo", MODE_PRIVATE);
        boolean switchstate = prefs.getBoolean("switch_status", false);
        if (switchstate == true) {
            myswitch.setChecked(true);

        } else {
            myswitch.setChecked(false);
        }

        //myswitch.setChecked(true);
        myswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = getSharedPreferences("edu.cmu.pocketsphinx.demo", MODE_PRIVATE).edit();
                editor.putBoolean("switch_status", myswitch.isChecked());
                editor.commit();
                if (isChecked == true) {

                    ((TextView) findViewById(R.id.caption_text))
                            .setText("Preparing the recognizer");

                    new AsyncTask < Void, Void, Exception > () {
                        @Override
                        protected Exception doInBackground(Void...params) {
                            try {
                                Assets assets = new Assets(PocketSphinxActivity.this);
                                File assetDir = assets.syncAssets();
                                setupRecognizer(assetDir);
                            } catch (IOException e) {
                                return e;
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Exception result) {
                            if (result != null) {
                                ((TextView) findViewById(R.id.caption_text))
                                        .setText("Failed to init recognizer " + result);
                                recognizer.shutdown();
                            } else {
                                switchSearch(KWS_SEARCH);
                            }
                        }
                    }.execute();

                } else {
                    try {
                        recognizer.cancel();
                        ((TextView) findViewById(R.id.caption_text))
                                .setText("");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.out.println("Recognizer is shutting down");

                }
            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
  /* if (!myswitch.isChecked()){
       recognizer.cancel();
   }*/
    }

    /**
     * In partial result we get quick updates about current hypothesis. In
     * keyword spotting mode we can react here, in other modes we need to wait
     * for final result in onResult.
     */
    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;
        final MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.inmymind);

        String text = hypothesis.getHypstr();
        if (text.equals(KEYPHRASE)) {
            //Toast.makeText(getApplicationContext(), "Got it", Toast.LENGTH_SHORT).show();
            recognizer.cancel();
            audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
            audioManager.setStreamVolume(AudioManager.STREAM_RING, 100, AudioManager.FLAG_ALLOW_RINGER_MODES | AudioManager.FLAG_PLAY_SOUND);
            mp.start();
            final boolean hasFlash = getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
            if (hasFlash) {
                camera= Camera.open();
                Camera.Parameters p=camera.getParameters();
                p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                camera.setParameters(p);
                camera.startPreview();
            }
            v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            v.vibrate(900000);
            AlertDialog alertDialog = new AlertDialog.Builder(PocketSphinxActivity.this).create();
            alertDialog.setTitle("Found your phone?");

            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if(hasFlash){
                                camera.stopPreview();
                                camera.release();
                            }
                            mp.stop();
                            mp.release();
                            v.cancel();
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            // startService(new Intent(this, AlertService.class));
   /*  Intent intent=getIntent();

     bool = intent.getStringExtra("bool");
     while (bool==null) {

     }
     if (bool.equals("true")) {
         mp.stop();
         v.cancel();
     }*/
            new java.util.Timer().schedule(new java.util.TimerTask() {
                                               @Override
                                               public void run() {
                                                   try {
                                                       switchSearch(KWS_SEARCH);
                                                   } catch (Exception e) {
                                                       e.printStackTrace();
                                                   }
                                               }
                                           }, 10000

            );


        } else
            ((TextView) findViewById(R.id.result_text)).setText(text);
    }


    /**
     * This callback is called when we stop the recognizer.
     */
    @Override
    public void onResult(Hypothesis hypothesis) {
        ((TextView) findViewById(R.id.result_text)).setText("");
        if (hypothesis != null) {
            String text = hypothesis.getHypstr();
            makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBeginningOfSpeech() {}

    /**
     * We stop recognizer here to get a final result
     */
    @Override
    public void onEndOfSpeech() {
        if (!recognizer.getSearchName().equals(KWS_SEARCH))
            switchSearch(KWS_SEARCH);
    }

    private void switchSearch(String searchName) {
        recognizer.stop();

        // If we are not spotting, start listening with timeout (10000 ms or 10 seconds).
        if (searchName.equals(KWS_SEARCH))
            recognizer.startListening(searchName);
        else
            recognizer.startListening(searchName, 10000);

        String caption = getResources().getString(captions.get(searchName));
        ((TextView) findViewById(R.id.caption_text)).setText(caption);
        myswitch.setClickable(true);
    }

    private void setupRecognizer(File assetsDir) throws IOException {
        // The recognizer can be configured to perform multiple searches
        // of different kind and switch between them
        myswitch.setClickable(false);
        recognizer = defaultSetup()
                .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                .setDictionary(new File(assetsDir, "cmudict-en-us.dict"))

                        // To disable logging of raw audio comment out this call (takes a lot of space on the device)
                //.setRawLogDir(assetsDir)

                        // Threshold to tune for keyphrase to balance between false alarms and misses
                .setKeywordThreshold(1e-45f)

                        // Use context-independent phonetic search, context-dependent is too slow for mobile
                .setBoolean("-allphone_ci", true)

                .getRecognizer();
        recognizer.addListener(this);


        // Create keyword-activation search.
        recognizer.addKeyphraseSearch(KWS_SEARCH, KEYPHRASE);


    }

    @Override
    public void onError(Exception error) {
        ((TextView) findViewById(R.id.caption_text)).setText(error.getMessage());
    }

    @Override
    public void onTimeout() {
        switchSearch(KWS_SEARCH);
    }






}
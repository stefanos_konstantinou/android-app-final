package edu.cmu.pocketsphinx.demo;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class History extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String path=getApplicationContext().getFilesDir().getAbsolutePath()+"/history.txt";
        File file =new File(path);
        getActionBar().setTitle("History");
        //checking to see if the history file exists to avoid errors
        if (file.exists()){
            //if the file exists we read the file in
            Scanner s = null;
            try {
                s = new Scanner(new File(path));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            ArrayList<String> list = new ArrayList<String>();
            while (s.hasNextLine()) {
                //we add line by line each location in array list
                list.add(s.nextLine());
            }
            s.close();


            setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_history,list));
            ListView lv=getListView();
            //list view is previewed with all locations from list arraylist
            lv.setTextFilterEnabled(true);
        }
        else{
            Toast.makeText(History.this, "No places in history yet!", Toast.LENGTH_SHORT).show();

        }

        ArrayAdapter<String> arrayadapter;




    }


}

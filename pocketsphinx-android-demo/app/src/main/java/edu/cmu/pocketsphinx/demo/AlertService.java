//This Alert service class is created in order to start a service and show an alert dialog from anywhere
// and wake up the PocketSphinxActivity . On dismiss click an intent is started and data are sent from here
// to PocketSphinxActivity in order to stop vibration and music when phone is found
//will be used in possible updates

package edu.cmu.pocketsphinx.demo;


import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.IBinder;
import android.view.WindowManager;

public class AlertService extends Service {

    String bool="false";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {



        final AlertDialog alertDialog = new AlertDialog.Builder(AlertService.this).create();
        alertDialog.setTitle("Found your phone?");
        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alertDialog.setButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bool = "true";
                if (bool.equals("true"))
                {
                    Intent in = new Intent(AlertService.this, PocketSphinxActivity.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    in.putExtra("bool", bool);
                    getApplicationContext().startActivity(in);
                }


            }
        });
        /**ADD THIS FOR DISPLAY THE ALERT ANYWHERE*/
        alertDialog.show();


        return flags;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}//End class
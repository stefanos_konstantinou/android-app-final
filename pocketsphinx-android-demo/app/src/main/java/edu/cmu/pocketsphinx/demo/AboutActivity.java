package edu.cmu.pocketsphinx.demo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AboutActivity extends Activity {
    String[] mobileArray = {"Privacy Policy","How does it work?","About us"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        // Creating a list view and adding mobile array as the options
        ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.activity_listview, mobileArray);
        final List<String> list =new ArrayList<String>();
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String temp= (String) ((TextView) view).getText();
               if (temp.equals("Privacy Policy")) {
                   //reading the privacy policy textfile and displaying it as an alert dialog
                   Scanner s=new Scanner(getResources().openRawResource(R.raw.privacypolicy));
                   while (s.hasNextLine()){
                       list.add(s.nextLine());
                   }
                   AlertDialog alertDialog = new AlertDialog.Builder(AboutActivity.this).create();
                   alertDialog.setTitle("Privacy Policy");
                   alertDialog.setMessage(list.toString());


                   alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                           new DialogInterface.OnClickListener() {
                               public void onClick(DialogInterface dialog, int which) {
                                   dialog.dismiss();
                               }
                           });
                   alertDialog.show();

               }
                else if (temp.equals("About us")){
                   AlertDialog alertDialog = new AlertDialog.Builder(AboutActivity.this).create();
                   alertDialog.setTitle("About us");
                   alertDialog.setMessage("\u00a9"+" 2016 Oh mighty phone Inc\n All rights reserved \n Version 1.00");


                   alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                           new DialogInterface.OnClickListener() {
                               public void onClick(DialogInterface dialog, int which) {
                                   dialog.dismiss();
                               }
                           });
                   alertDialog.show();
               }
                else if(temp.equals("How does it work?")){
                   //reading in the how it works text file and displaying it as an alert dialog
                   Scanner s=new Scanner(getResources().openRawResource(R.raw.howitworks));
                   while (s.hasNextLine()){
                       list.add(s.nextLine());
                   }
                   AlertDialog alertDialog = new AlertDialog.Builder(AboutActivity.this).create();
                   alertDialog.setTitle("How does it work?");
                   alertDialog.setMessage(list.toString());


                   alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                           new DialogInterface.OnClickListener() {
                               public void onClick(DialogInterface dialog, int which) {
                                   dialog.dismiss();
                               }
                           });
                   alertDialog.show();
               }
            }
        });
    }
}



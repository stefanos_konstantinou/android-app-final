package edu.cmu.pocketsphinx.demo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;



public class SMSBroadcastReceiver extends BroadcastReceiver {

    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final String TAG = "SMSBroadcastReceiver";
    public static String message;


    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Log.i(TAG, "Intent recieved: " + intent.getAction());

            if (intent.getAction().equals(SMS_RECEIVED)) {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    //  SmsMessage messages = new SmsMessage[pdus.length];
                    for (int i = 0; i < pdus.length; i++) {
                        SmsMessage messages = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        message = messages.getDisplayMessageBody();

                        Intent in = new Intent(context, MainActivity.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        in.putExtra("message", message);
                        context.startActivity(in);

                        //Toast.makeText(context, "Message recieved: " + messages.getMessageBody(), Toast.LENGTH_LONG).show();

                    }

                }

            }

        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }
}



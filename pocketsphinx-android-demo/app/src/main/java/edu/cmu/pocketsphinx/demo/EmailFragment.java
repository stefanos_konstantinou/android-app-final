package edu.cmu.pocketsphinx.demo;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileOutputStream;


public class EmailFragment extends Fragment {
    String email;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Email();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_email, container, false);
    }
    private void Email() {
        Button EmailSub = (Button) getActivity().findViewById(R.id.EmailBtn);
        EmailSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //on click of button we check if the email is valid ,
                //and if it is we save it locally in a text file for further use
                try {
                    EditText Uemail = (EditText) getActivity().findViewById(R.id.UserEmail);
                    email = Uemail.getText().toString();
                    if (email.isEmpty()) {
                        Toast.makeText(getActivity().getApplicationContext(), "Please enter an emergency E-mail!", Toast.LENGTH_SHORT).show();
                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) // Pattern that checks for possible mistakes
                    {
                        Toast.makeText(getActivity().getApplicationContext(), "Wrong E-mail syntax given!", Toast.LENGTH_SHORT).show();
                    } else // If email is right is set as default emergency email
                    {
                        Toast.makeText(getActivity().getApplicationContext(), "Emergency email is now set!", Toast.LENGTH_SHORT).show();
                        FileOutputStream OutputStream;
                        OutputStream = getActivity().openFileOutput("emergemail.txt", Context.MODE_PRIVATE);
                        OutputStream.write(email.getBytes());
                        OutputStream.close();
                        Uemail.setText("");
                    }
                } catch (Exception e) {
                    Toast.makeText(getActivity().getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }


}
package edu.cmu.pocketsphinx.demo;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

public class VoiceRecTest extends Fragment {
    private static final int VOICE_RECOGNITION_RESULT = 1000;
    String MagicWord = "oh mighty computer";
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VoiceRec();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_voice_rec_test, container, false);
    }

    private void VoiceRec() {
        Button voice = (Button) getActivity().findViewById(R.id.voicebtn);
        voice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
                try {
                    startActivityForResult(intent, VOICE_RECOGNITION_RESULT);
                } catch (Exception e) {
                    Toast.makeText(getActivity().getApplicationContext(), "Error initializing speech engine " + e, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case VOICE_RECOGNITION_RESULT:
                //do stuff with voice recognition
                if (resultCode == getActivity().RESULT_OK && data != null) {

                    try {
                        ArrayList < String > result = data
                                .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                        String mostLikelyThingHeard = result.get(0);
                        if (mostLikelyThingHeard.equalsIgnoreCase(MagicWord)) {
                            Toast.makeText(getActivity().getApplicationContext(), "Word matches ", Toast.LENGTH_LONG).show();

                        } else if (!mostLikelyThingHeard.equals(MagicWord)) {
                            Toast.makeText(getActivity().getApplicationContext(), "You said " + mostLikelyThingHeard + " instead of " + MagicWord, Toast.LENGTH_LONG).show();
                        }


                    } catch (Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(), "Error in recognizer", Toast.LENGTH_LONG).show();
                    }

                }
                break;
            default:
                break;
        }
    }


}
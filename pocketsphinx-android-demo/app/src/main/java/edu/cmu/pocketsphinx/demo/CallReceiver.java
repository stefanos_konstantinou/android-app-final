//Call receiver class will be used in further updates in order
//to get the action when the microphone is requested for use

package edu.cmu.pocketsphinx.demo;

import android.content.Context;
import android.widget.Toast;

import java.util.Date;

public  class CallReceiver extends PhonecallReceiver {

    @Override
    protected void onIncomingCallReceived(Context ctx, String number, Date start)
    {

    }

    @Override
    protected  void onIncomingCallAnswered(Context ctx, String number, Date start)
    {

    }

    @Override
    protected  void onIncomingCallEnded(Context ctx, String number, Date start, Date end)
    {
        Toast.makeText(ctx,"Call Ended",Toast.LENGTH_LONG).show();
    }

    @Override
    protected  void onOutgoingCallStarted(Context ctx, String number, Date start)
    {
        //
    }

    @Override
    protected  void onOutgoingCallEnded(Context ctx, String number, Date start, Date end)
    {
        //
    }

    @Override
    protected  void onMissedCall(Context ctx, String number, Date start)
    {
        //
    }

}
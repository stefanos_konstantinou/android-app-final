//future update will make the whole application act like a
//background service to avoid any errors if the users accidentally stops it

package edu.cmu.pocketsphinx.demo;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

public class MyService extends Service {

    @Override
    public  int onStartCommand(Intent intent,int flags,int startId){
        return  Service.START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

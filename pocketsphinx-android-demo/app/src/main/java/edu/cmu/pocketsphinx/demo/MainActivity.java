package edu.cmu.pocketsphinx.demo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    String s = null;
    Boolean music;
    String MagicWord;

    String LocationLink;
    GPSTracker gps;
    String filename = "magicword.txt";
    String message;
    Vibrator v;
    AudioManager audioManager;
    String email;
    String user = "droidapptest94@gmail.com";
    String pass = "androidtest"; //enter password

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.idLayout);
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setFillAfter(true);
        animation.setDuration(1200);
        //apply the animation ( fade In ) to your LAyout
        layout.startAnimation(animation);

        handleOpenSetup();
        handleOpenRecognizer();
        handleOpenCommands();
        handleOpenHistory();
        onReceivingSMS();
        onAboutClick();
        PackageManager m = getPackageManager();
        s = getPackageName();
        PackageInfo p = null;
        try {
            p = m.getPackageInfo(s, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        s = p.applicationInfo.dataDir;
        //onBackPressed();
        System.out.println(s);



        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if ((manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) == false) {
            buildAlertMessageNoGps();
        }

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MyService.class);
        i.putExtra("KEY1", "Value to be used by the service");
        getApplicationContext().startService(i);
        moveTaskToBack(true);
    }


 /*  @Override
   protected void onDestroy(){
       super.onDestroy();
       Intent i=new Intent(getApplicationContext(),MyService.class);
       i.putExtra("KEY1","Value to be used by the service");
       getApplicationContext().startService(i);
   }*/

    private void handleOpenSetup() {
        Button buttonOpenOptions = (Button) findViewById(R.id.optionsbtn);
        buttonOpenOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Setup_window.class);
                MainActivity.this.startActivity(intent);
            }
        });
    }

    private void handleOpenCommands() {
        Button buttonOpenOptions = (Button) findViewById(R.id.commandsbtn);
        buttonOpenOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("Commands");
                alertDialog.setMessage("Recognizer application supports the command oh mighty computer.Further updates" +
                        " will support the option of setting your own command at which your phone will obey!");


                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        });
    }


    private void handleOpenRecognizer() {
        Button buttonOpenOptions = (Button) findViewById(R.id.setRecognbtn);
        buttonOpenOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PocketSphinxActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });
    }

    private void handleOpenHistory() {
        Button buttonOpenOptions = (Button) findViewById(R.id.Historybtn);
        buttonOpenOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, History.class);
                MainActivity.this.startActivity(intent);
            }
        });
    }
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Enable your GPS to increase chances of finding your phone?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        dialog.cancel();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void onAboutClick() {
        Button buttonOpenAbout = (Button) findViewById(R.id.aboutbtn);
        buttonOpenAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AboutActivity.class);
                MainActivity.this.startActivity(intent);            }
        });
    }


    public void onReceivingSMS() {
        message="";
        Intent intent = getIntent();
        message = intent.getStringExtra("message");
        // Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

        if (message != null) {

            FileInputStream fis = null;
            try {
                fis = getApplicationContext().openFileInput("magicword.txt");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            try {
                while ((line = bufferedReader.readLine()) != null) {
                    sb.append(line);
                    line = sb.toString();
                    if (message.equalsIgnoreCase(line)) {
                        //TURNING ON WIFI
                        WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                        wifiManager.setWifiEnabled(true);

                        //// TURNING ON MOBILE DATA //////
                        ConnectivityManager datamanager;
                        datamanager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        try {
                            Method dataMtd = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
                            dataMtd.setAccessible(true);
                            try {
                                dataMtd.invoke(datamanager, true);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        }

                        File file = getBaseContext().getFileStreamPath("musicbool.txt");
                        FileInputStream fileInputStream = null;
                        if (file.exists()) {
                            try {
                                fileInputStream = getApplicationContext().openFileInput("musicbool.txt");
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                            InputStreamReader inputstreamreader = new InputStreamReader(fileInputStream);
                            BufferedReader bufferedReaderb = new BufferedReader(inputstreamreader);
                            StringBuilder sba = new StringBuilder();
                            String lineb = null;
                            try {
                                while ((lineb = bufferedReaderb.readLine()) != null) {
                                    sba.append(lineb);
                                }
                                lineb = sba.toString();
                                //CHECKING TO SEE IF MUSIC IS ENABLED IN THIS ACTION
                                if (lineb.equals("1")) {
                                    music = true;
                                } else  {
                                    music = false;
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        //set sound and ringtone to loud mode
                        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
                        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
                        audioManager.setStreamVolume(AudioManager.STREAM_RING, 100, AudioManager.FLAG_ALLOW_RINGER_MODES | AudioManager.FLAG_PLAY_SOUND);
                        if (music!=null && music) {
                            final MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.inmymind);

                            mp.start();
                            v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
                            // Vibrate for AS long as possible until stopped
                            v.vibrate(900000);
                            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                            alertDialog.setTitle("Found your phone?");

                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "YES",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            mp.stop();
                                            mp.release();
                                            v.cancel();
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }

                        //Toast.makeText(getApplicationContext(), " Magic Word matches message received  ", Toast.LENGTH_LONG).show();
                        FileInputStream fisi = null;
                        try {
                            fisi = getApplicationContext().openFileInput("emergemail.txt");
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        InputStreamReader isra = new InputStreamReader(fisi);
                        BufferedReader bufferedReadera = new BufferedReader(isra);
                        StringBuilder sba = new StringBuilder();
                        String linea = null;
                        try {
                            while ((linea = bufferedReadera.readLine()) != null) {
                                sba.append(linea);
                            }
                            linea = sba.toString();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        new java.util.Timer().schedule(new java.util.TimerTask() {
                                                           @Override
                                                           public void run() {

                                                               try {

                                                               } catch (Exception e) {
                                                                   e.printStackTrace();
                                                               }
                                                           }
                                                       }, 10000

                        );
                        if (linea != null) {
                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            gps = new GPSTracker(MainActivity.this);
                            if (gps.canGetLocation()) {
                                try {
                                    Thread.sleep(6000);  //suspending the process for 6 seconds to give time to
                                                        //get a connection with the Google  maps server
                                } catch(InterruptedException ex) {
                                    Thread.currentThread().interrupt();
                                }
                                //getting the location not only for a google maps use but as a full address in case
                                //something goes wrong with the link construction
                                double latitude = gps.getLatitude();
                                double longitude = gps.getLongitude();
                                List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
                                String _Location = addressList.get(0).getAddressLine(0);
                                _Location = _Location + " " + addressList.get(0).getCountryName();
                                _Location = _Location + " " + addressList.get(0).getPostalCode();
                                //Toast.makeText(getApplicationContext(), _Location, Toast.LENGTH_LONG).show();
                                email = linea;
                                LocationLink = "http://www.google.com/maps/place/" + latitude + "," + longitude + "\n" + "Full address is : " +_Location;
                                final MailSender mail = new MailSender(user, pass, email, LocationLink);
                                //giving some time for the internet to be connected and then send the constructed email
                                new java.util.Timer().schedule(new java.util.TimerTask() {
                                                                   @Override
                                                                   public void run() {

                                                                       try {
                                                                           mail.send();
                                                                       } catch (Exception e) {
                                                                           e.printStackTrace();
                                                                       }
                                                                   }
                                                               }, 10000
                                );
                                FileOutputStream OutputStream;
                                String path=getApplicationContext().getFilesDir().getAbsolutePath()+"/history.txt";
                                File file1 =new File(path);
                                //if the history file does not exist create it.
                                // write the location in history text file
                                try {
                                    if (file1.exists()){
                                        OutputStream = getApplicationContext().openFileOutput("history.txt", Context.MODE_APPEND);
                                        OutputStream.write('\n');
                                        OutputStream.write(_Location.getBytes());
                                    }else {
                                        OutputStream = getApplicationContext().openFileOutput("history.txt", Context.MODE_PRIVATE);
                                        OutputStream.write(_Location.getBytes());
                                    }
                                    OutputStream.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                //Toast.makeText(getApplicationContext(), "Email Sent!", Toast.LENGTH_LONG).show();
                                Intent startMain = new Intent(Intent.ACTION_MAIN);
                                startMain.addCategory(Intent.CATEGORY_HOME);
                                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(startMain);

                            } else {
                                // can't get location
                                // GPS or Network is not enabled
                                // Ask user to enable GPS/network in settings
                                gps.showSettingsAlert();
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_LONG).show();
                        }
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}